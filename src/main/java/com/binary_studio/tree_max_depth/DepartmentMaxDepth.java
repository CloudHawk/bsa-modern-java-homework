package com.binary_studio.tree_max_depth;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}
		Set<Integer> arr = new HashSet<>();
		recDepth(rootDepartment, 1, arr);
		//I think there still will be problems with stack
		return Collections.max(arr);
	}

	public static void recDepth(Department curDepartment, Integer depth, Set<Integer> arr) {
		if (curDepartment.isEnd()) {
			arr.add(depth);
		}
		else {
			for (Department dep : curDepartment.subDepartments) {
				if (dep == null) {
					continue;
				}
				recDepth(dep, depth+1, arr);
			}
		}
	}

}
