package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private String name;

	private boolean flag = true;

	private final PositiveInteger shieldHP;

	private PositiveInteger curShieldHP;

	private final PositiveInteger hullHP;

	private PositiveInteger curHullHP;

	private final PositiveInteger capacitor;

	private PositiveInteger curCapacitor;

	private PositiveInteger capacitorRegeneration;

	private PositiveInteger pg;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger capacitor,
			PositiveInteger capacitorRegeneration, PositiveInteger pg, PositiveInteger speed, PositiveInteger size,
			AttackSubsystem attackSubsystem, DefenciveSubsystem defenciveSubsystem) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.capacitor = capacitor;
		this.capacitorRegeneration = capacitorRegeneration;
		this.pg = pg;
		this.speed = speed;
		this.size = size;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
	}

	@Override
	public void endTurn() {
		// TODO: Ваш код здесь :)
		if (this.curCapacitor.value() < this.capacitor.value()) {
			this.curCapacitor = this.curCapacitor.value() + this.capacitorRegeneration.value() > this.capacitor.value()
					? PositiveInteger.of(this.capacitor.value())
					: PositiveInteger.of(this.curCapacitor.value() + this.capacitorRegeneration.value());
		}

	}

	@Override
	public void startTurn() {
		// TODO: Ваш код здесь :)
		if (this.flag) {
			this.curCapacitor = this.capacitor;
			this.curHullHP = this.hullHP;
			this.curShieldHP = this.shieldHP;
			this.flag = false;
		}

	}

	@Override
	public String getName() {
		// TODO: Ваш код здесь :)
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		// TODO: Ваш код здесь :)
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		// TODO: Ваш код здесь :)
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		// TODO: Ваш код здесь :)
		if (this.curCapacitor.value() >= this.attackSubsystem.getCapacitorConsumption().value()) {
			this.curCapacitor = PositiveInteger
					.of(this.curCapacitor.value() - this.attackSubsystem.getCapacitorConsumption().value());
			PositiveInteger damage = this.attackSubsystem.attack(target);

			return Optional.of(new AttackAction(damage, this, target, this.attackSubsystem));
		}
		else {
			return Optional.empty();
		}
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		// TODO: Ваш код здесь :)
		AttackAction reducedAttack = this.defenciveSubsystem.reduceDamage(attack);
		if (this.curShieldHP.value() >= reducedAttack.damage.value()) {
			this.curShieldHP = PositiveInteger.of(this.curShieldHP.value() - reducedAttack.damage.value());
		}
		else if (this.curHullHP.value() + this.curShieldHP.value() - reducedAttack.damage.value() > 0) {
			this.curHullHP = PositiveInteger
					.of(this.curHullHP.value() + this.curShieldHP.value() - reducedAttack.damage.value());
		}
		else {
			this.curHullHP = PositiveInteger.of(0);
			return new AttackResult.Destroyed();
		}
		return new AttackResult.DamageRecived(reducedAttack.weapon, reducedAttack.damage, reducedAttack.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		// TODO: Ваш код здесь :)
		RegenerateAction ra = this.defenciveSubsystem.regenerate();
		PositiveInteger shieldRegenerated;
		PositiveInteger hullRegenerated;
		if (this.curCapacitor.value() >= this.defenciveSubsystem.getCapacitorConsumption().value()) {
			this.curCapacitor = PositiveInteger
					.of(this.curCapacitor.value() - this.defenciveSubsystem.getCapacitorConsumption().value());

			if (this.curShieldHP.value() + ra.shieldHPRegenerated.value() > this.shieldHP.value()) {
				shieldRegenerated = PositiveInteger.of(this.shieldHP.value() - this.curShieldHP.value());
				this.curShieldHP = PositiveInteger.of(this.shieldHP.value());
			}
			else {
				shieldRegenerated = PositiveInteger.of(ra.shieldHPRegenerated.value());
				this.curShieldHP = PositiveInteger.of(this.curShieldHP.value() + ra.shieldHPRegenerated.value());
			}

			if (this.curHullHP.value() + ra.hullHPRegenerated.value() > this.hullHP.value()) {
				hullRegenerated = PositiveInteger.of(this.hullHP.value() - this.curHullHP.value());
				this.curHullHP = PositiveInteger.of(this.hullHP.value());
			}
			else {
				hullRegenerated = PositiveInteger.of(ra.hullHPRegenerated.value());
				this.curHullHP = PositiveInteger.of(this.curHullHP.value() + ra.hullHPRegenerated.value());
			}

			return Optional.of(new RegenerateAction(shieldRegenerated, hullRegenerated));
		}
		else {
			return Optional.empty();
		}

	}

}
