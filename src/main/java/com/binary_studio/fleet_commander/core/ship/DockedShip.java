package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger capacitor;

	private PositiveInteger capacitorRegeneration;

	private PositiveInteger pg;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger capacitor,
			PositiveInteger capacitorRegeneration, PositiveInteger pg, PositiveInteger speed, PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.capacitor = capacitor;
		this.capacitorRegeneration = capacitorRegeneration;
		this.pg = pg;
		this.speed = speed;
		this.size = size;
	}

	public DockedShip() {
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		// TODO: Ваш код здесь :)
		return new DockedShip(name, shieldHP, hullHP, capacitorAmount, capacitorRechargeRate, powergridOutput, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		// TODO: Ваш код здесь :)
		if (subsystem == null) {
			this.attackSubsystem = null;
			return;
		}
		Integer curPG = 0;
		if (this.defenciveSubsystem != null) {
			curPG += this.defenciveSubsystem.getPowerGridConsumption().value();
		}
		if (subsystem.getPowerGridConsumption().value() + curPG > this.pg.value()) {
			throw new InsufficientPowergridException(
					subsystem.getPowerGridConsumption().value() + curPG - this.pg.value());
		}
		this.attackSubsystem = subsystem;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		// TODO: Ваш код здесь :)
		if (subsystem == null) {
			this.defenciveSubsystem = null;
			return;
		}
		Integer curPG = 0;
		if (this.attackSubsystem != null) {
			curPG += this.attackSubsystem.getPowerGridConsumption().value();
		}
		if (subsystem.getPowerGridConsumption().value() + curPG > this.pg.value()) {
			throw new InsufficientPowergridException(
					subsystem.getPowerGridConsumption().value() + curPG - this.pg.value());
		}
		this.defenciveSubsystem = subsystem;
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		// TODO: Ваш код здесь :)
		if (this.attackSubsystem == null && this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		else if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		else {
			return new CombatReadyShip(this.name, this.shieldHP, this.hullHP, this.capacitor,
					this.capacitorRegeneration, this.pg, this.speed, this.size, this.attackSubsystem,
					this.defenciveSubsystem);
		}
	}

}
