package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private String name;

	private PositiveInteger impactReduction;

	private PositiveInteger shieldRegen;

	private PositiveInteger hullRegen;

	private PositiveInteger capacitorUsage;

	private PositiveInteger pgRequirement;

	public DefenciveSubsystemImpl() {
	}

	public DefenciveSubsystemImpl(String name, PositiveInteger impactReduction, PositiveInteger shieldRegen,
			PositiveInteger hullRegen, PositiveInteger capacitorUsage, PositiveInteger pgRequirement) {
		this.name = name;
		this.impactReduction = impactReduction;
		this.shieldRegen = shieldRegen;
		this.hullRegen = hullRegen;
		this.capacitorUsage = capacitorUsage;
		this.pgRequirement = pgRequirement;
	}

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		// TODO: Ваш код здесь :)
		if (name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new DefenciveSubsystemImpl(name, impactReductionPercent, shieldRegeneration, hullRegeneration,
				capacitorConsumption, powergridConsumption);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		// TODO: Ваш код здесь :)
		return this.pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		// TODO: Ваш код здесь :)
		return this.capacitorUsage;
	}

	@Override
	public String getName() {
		// TODO: Ваш код здесь :)
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		// TODO: Ваш код здесь :)
		if (incomingDamage.damage.value() != 0) {
			if (this.impactReduction.value() > 95) {
				return new AttackAction(
						incomingDamage.damage.value() * 0.05 < 1 ? new PositiveInteger(1)
								: new PositiveInteger((int) Math.ceil(incomingDamage.damage.value() * 0.05)),
						incomingDamage.attacker, incomingDamage.target, incomingDamage.weapon);
			}
			else {
				return new AttackAction(
						incomingDamage.damage.value() * (1 - this.impactReduction.value() / 100.0) < 1
								? new PositiveInteger(1)
								: new PositiveInteger((int) Math.ceil(
										incomingDamage.damage.value() * (1 - this.impactReduction.value() / 100.0))),
						incomingDamage.attacker, incomingDamage.target, incomingDamage.weapon);
			}
		}
		return incomingDamage;
	}

	@Override
	public RegenerateAction regenerate() {
		// TODO: Ваш код здесь :)
		return new RegenerateAction(this.shieldRegen, this.hullRegen);
	}

}
