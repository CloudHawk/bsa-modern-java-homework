package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

import static java.lang.Double.min;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger baseDamage;

	private PositiveInteger optimalSize;

	private PositiveInteger optimalSpeed;

	private PositiveInteger capacitorUsage;

	private PositiveInteger pgRequirement;

	public AttackSubsystemImpl() {
	}

	public AttackSubsystemImpl(String name, PositiveInteger baseDamage, PositiveInteger optimalSize,
			PositiveInteger optimalSpeed, PositiveInteger capacitorUsage, PositiveInteger pgRequirement) {
		this.name = name;
		this.baseDamage = baseDamage;
		this.optimalSize = optimalSize;
		this.optimalSpeed = optimalSpeed;
		this.capacitorUsage = capacitorUsage;
		this.pgRequirement = pgRequirement;
	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		// TODO: Ваш код здесь :)
		if (name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new AttackSubsystemImpl(name, baseDamage, optimalSize, optimalSpeed, capacitorConsumption,
				powergridRequirments);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		// TODO: Ваш код здесь :)

		return this.pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		// TODO: Ваш код здесь :)
		return this.capacitorUsage;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		// TODO: Ваш код здесь :)
		double sizeReductionModifier = target.getSize().value() >= this.optimalSize.value() ? 1.0
				: target.getSize().value().doubleValue() / this.optimalSize.value();
		double speedReductionModifier = target.getCurrentSpeed().value() <= this.optimalSpeed.value() ? 1.0
				: this.optimalSpeed.value().doubleValue() / (2 * target.getCurrentSpeed().value());
		double damage = Math.ceil(this.baseDamage.value() * min(sizeReductionModifier, speedReductionModifier));
		return new PositiveInteger((int)damage);
	}

	@Override
	public String getName() {
		// TODO: Ваш код здесь :)
		return this.name;
	}

}
