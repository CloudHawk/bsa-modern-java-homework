package com.binary_studio.uniq_in_sorted_stream;

//You CAN modify this class
public final class Row<RowData> {

	private final Long id;
	private static Long idCheck = null;
	private static boolean flag = true;

	public Row(Long id) {
		this.id = id;
	}

	public Long getPrimaryId() {
		return this.id;
	}

	public boolean check(){
		if(!idCheck.equals(this.id)){
			idCheck=id;
			flag=true;
		} else {
			flag = false;
		}
		return flag;
	}

}
